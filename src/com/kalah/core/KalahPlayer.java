package com.kalah.core;

import java.util.ArrayList;

/**
 * Represents one of the players and his/her side of the board.
 *
 */
public class KalahPlayer {

    private final String name;
    private final ArrayList<KalahPit> pits;
    private final KalahHouse house;

    /**
     * Constructor, uses game mode to instantiate pits with initial stones
     *
     * @param name the name of the player
     * @param mode game mode determines how many stones will start at each pit
     * @param pitsNumber The number of pits the player has
     */
    public KalahPlayer(String name, KalahMode mode, Integer pitsNumber) {
	this.name = name;
	pits = new ArrayList<>();
	house = new KalahHouse();
	for (int i = 0; i < pitsNumber; i++) {
	    KalahPit pit = new KalahPit(mode);
	    pits.add(pit);
	}
    }

    /**
     * Gets the player's name
     *
     * @return the name of the player
     */
    public String getName() {
	return name;
    }

    /**
     * Gets a pit of this player by its index
     *
     * @param pitIndex the corresponding index of the pit, a value between 0 and KalahGame.PITS
     * @return the chosen pit or null if the index is out of range
     */
    public KalahPit getPit(Integer pitIndex) {
	if (pitIndex < 0 || pitIndex >= pits.size()) {
	    return null;
	}
	return pits.get(pitIndex);
    }

    /**
     * Gets the house of the player
     *
     * @return the player's house
     */
    public KalahHouse getHouse() {
	return house;
    }

    /**
     * Check if the plater has any remaining stones
     *
     * @return true if the player has stones in any of the pits, false otherwise
     */
    protected Boolean hasStones() {
	for (KalahPit pit : pits) {
	    if (pit.size() > 0) {
		return true;
	    }
	}
	return false;
    }

    /**
     * Gets all the pits from the player in the indexed order
     *
     * @return an array of pits
     */
    public ArrayList<KalahPit> getPits() {
	return pits;
    }

}
