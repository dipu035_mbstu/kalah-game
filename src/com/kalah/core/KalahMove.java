package com.kalah.core;

/**
 *
 * Identifies the type of movement performed by a player.
 *
 */
public enum KalahMove {

    /**
     * The intended move was illegal
     */
    ILEGAL,
    /**
     * The intended move was ok, the game must continue
     */
    CONTINUE,
    /**
     * The intended move created a steal situation, the game must continue
     */
    STEAL,
    /**
     * The intended move ended up in the house, the player has an additional turn
     */
    PLAYAGAIN;

    KalahMove() {
    }

}
