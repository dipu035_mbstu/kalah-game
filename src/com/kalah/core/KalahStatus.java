package com.kalah.core;


/**
 * Identifies the current game status.
 *
 */
public enum KalahStatus {

    /**
     * Game was instantiated but not started yet
     */
    INIT,
    /**
     * Game started, Player A is on turn
     */
    PLAYINGA,
    /**
     * Game started, Player B is on turn
     */
    PLAYINGB,
    /**
     * Game has finished
     */
    FINISHED;

    KalahStatus() {
    }

}
