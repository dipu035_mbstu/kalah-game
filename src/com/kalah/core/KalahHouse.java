package com.kalah.core;

import java.util.ArrayList;

/**
 * Represents the house or kalah where the players accumulate the stones.
 *
 */
public class KalahHouse {

    private final ArrayList<KalahStone> stones;

    /**
     * Constructor
     */
    public KalahHouse() {
	this.stones = new ArrayList<>();
    }

    /**
     * Gets how many stones are in this house
     *
     * @return the number of stones in this house
     */
    public Integer size() {
	return stones.size();
    }

    /**
     * Adds a stone to the house
     *
     * @param stone the stone to add to the house
     */
    protected void addStone(KalahStone stone) {
	stones.add(stone);
    }

}
