package com.kalah.core;


import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * Represents a pit of the board.
 *
 */
public class KalahPit {

    private final ArrayList<KalahStone> stones;

    /**
     * Constructor uses game mode the add the initial stones
     *
     * @param mode the mode determines how many stones put initialy in the pit
     */
    public KalahPit(KalahMode mode) {
	this.stones = new ArrayList<>();
	for (int i = 0; i < mode.stones(); i++) {
	    KalahStone stone = new KalahStone();
	    this.stones.add(stone);
	}
    }

    /**
     * Gets the amount of stones in this pit
     *
     * @return the number of stones
     */
    public Integer size() {
	return stones.size();
    }

    /**
     * Check if there are no remaining stones in the pit
     *
     * @return true if there is no stones, false otherwise
     */
    public Boolean isEmpty() {
	return stones.isEmpty();
    }

    /**
     * Takes and removes the stones from the pit. Creates a queue and returns it.
     *
     * @return all the stones as a LinkedList
     */
    protected LinkedList<KalahStone> takeStones() {
	LinkedList<KalahStone> takedStones = new LinkedList<>(stones);
	stones.clear();
	return takedStones;
    }

    /**
     * Adds a stone to the pit
     *
     * @param stone the stone to add
     */
    protected void addStone(KalahStone stone) {
	stones.add(stone);
    }

}
