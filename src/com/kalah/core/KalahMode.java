package com.kalah.core;

/**
 *
 * Different modes for the game that change the ammount of stones used initially per pit.
 *
 */
public enum KalahMode {

    /**
     * 3-stone Kalah
     */
    STONES_3(3),
    /**
     * 4-stone Kalah
     */
    STONES_4(4),
    /**
     * 6-stone Kalah
     */
    STONES_6(6);

    private final Integer stones;

    KalahMode(Integer stones) {
	this.stones = stones;
    }

    /**
     * Gets the number of stones to add to each pit under this mode
     *
     * @return the number of stones
     */
    public Integer stones() {
	return this.stones;
    }

}
