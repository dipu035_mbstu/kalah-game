package com.kalah.example;

import com.kalah.core.KalahGame;
import com.kalah.core.KalahMode;
import com.kalah.core.KalahPit;
import com.kalah.core.KalahPlayer;
import com.kalah.core.KalahStatus;
import static com.kalah.core.KalahStatus.FINISHED;
import static com.kalah.core.KalahStatus.INIT;
import static com.kalah.core.KalahStatus.PLAYINGA;
import static com.kalah.core.KalahStatus.PLAYINGB;
import com.kalah.exception.KalahException;
import com.kalah.exception.KalahIlegalMoveException;
import java.io.PrintStream;
import java.util.Scanner;

public class KalahConsole {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	try (Scanner in = new Scanner(System.in)) {
	    try {
		KalahGame game = new KalahGame(KalahMode.STONES_6);
		//print(game, System.out);
		//in.next();
		game.start();
		print(game, System.out);
		while (true) {
		    int n = in.nextInt();
		    try {
			game.move(n);
		    } catch (KalahIlegalMoveException kalahIllegalMoveException) {
			System.out.println("Illegal Move");
			continue;
		    }
		    print(game, System.out);
		    if (game.getStatus() == KalahStatus.FINISHED) {
			break;
		    }
		}
	    } catch (KalahException ex) {
		ex.printStackTrace();
	    }
	}
    }

    private static void print(KalahGame game, PrintStream stream) throws KalahException {
	switch (game.getStatus()) {
	    case INIT: {
		stream.println("Enter a number to start...");
		break;
	    }
	    case PLAYINGA: {
		printKalahGame(game, stream, true);
		stream.println("Player A Moves");
		break;
	    }
	    case PLAYINGB: {
		printKalahGame(game, stream, true);
		stream.println("Player B Moves");
		break;
	    }
	    case FINISHED: {
		printKalahGame(game, stream, false);
		KalahPlayer winer = game.getWinner();
		if (winer != null) {
		    stream.println(String.format("Finished. %s wins.", winer.getName()));
		} else {
		    stream.println(String.format("Finished with a tie."));
		}
		break;
	    }
	}
    }

    private static void printKalahGame(KalahGame game, PrintStream stream, Boolean withNumbers) {
	String houseA = String.format("[%s]", String.format("%02d", game.getPlayerA().getHouse().size()));
	String pitsA = "";
	for (KalahPit pit : game.getPlayerA().getPits()) {
	    pitsA = pitsA + String.format("(%d)", pit.size());
	}
	String houseB = String.format("[%s]", String.format("%02d", game.getPlayerB().getHouse().size()));
	String pitsB = "";
	for (KalahPit pit : game.getPlayerB().getPits()) {
	    pitsB = String.format("(%d)", pit.size()) + pitsB;
	}
	if (!withNumbers) {
	    stream.println(String.format("%s %s %s", game.getPlayerB().getName(), houseB, pitsB));
	    stream.println(String.format("%s     %s %s", game.getPlayerA().getName(), pitsA, houseA));
	} else {
	    String pitsnumbersA = "";
	    String pitsnumbersB = "";
	    for (int i = 0; i < game.getPlayerA().getPits().size(); i++) {
		pitsnumbersA = pitsnumbersA + String.format("(%d)", i);
	    }
	    for (int i = 0; i < game.getPlayerB().getPits().size(); i++) {
		pitsnumbersB = String.format("(%d)", i) + pitsnumbersB;
	    }
	    stream.println(String.format("              %s", pitsnumbersB));
	    stream.println(String.format("%s %s %s", game.getPlayerB().getName(), houseB, pitsB));
	    stream.println(String.format("%s      %s %s", game.getPlayerA().getName(), pitsA, houseA));
	    stream.println(String.format("              %s", pitsnumbersA));
	}

    }

}
